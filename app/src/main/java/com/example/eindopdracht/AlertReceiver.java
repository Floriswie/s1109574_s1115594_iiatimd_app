package com.example.eindopdracht;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.icu.text.Collator;
import android.media.Ringtone;
import android.util.Log;
import android.view.View;
import android.widget.Button;


import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.preference.PreferenceManager;

public class AlertReceiver extends BroadcastReceiver {



    @Override
    public void onReceive(Context context, Intent intent) {


        NotificationHelper notificationHelper = new NotificationHelper(context);
        NotificationCompat.Builder nb = notificationHelper.getChannelNotification();
        notificationHelper.getManager().notify(1, nb.build());
        notificationHelper.playSound();


        Intent localIntent = new Intent("alarmRing");
        localIntent.putExtra("alarmWentOff", true);
        LocalBroadcastManager.getInstance(context).sendBroadcast(localIntent);
    }



}