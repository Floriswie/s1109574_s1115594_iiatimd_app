package com.example.eindopdracht;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class DateTime{

    @ColumnInfo
    private String hours_slept;

    @ColumnInfo
    private String created_at;

    @PrimaryKey
    @NonNull
    private String id;

    public DateTime (String hours_slept, String created_at, String id){
        this.hours_slept = hours_slept;
        this.created_at = created_at;
        this.id = id;
    }


    public String getHours_slept() {
        return hours_slept;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getId(){
        return id;
    }

}
