package com.example.eindopdracht;

import android.util.Log;


public class InsertDateTimeTask implements Runnable{
    AppDatabase db;
    DateTime dateTime;



    public InsertDateTimeTask(AppDatabase db, DateTime dateTime){
        this.db = db;
        this.dateTime = dateTime;


    }

    @Override
    public void run() {
        Log.d("gefaald", dateTime.getCreated_at());

        db.DateTimeDao().InsertDateTime(this.dateTime);

    }

//        db.DateTimeDao().InsertDateTime(this.dateTime);
//        String name = db.DateTimeDao().getAll().get(0).getName();
//        Log.d("naInsert", name);

}
