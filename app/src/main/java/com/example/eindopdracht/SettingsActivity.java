package com.example.eindopdracht;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.preference.DropDownPreference;
import androidx.preference.EditTextPreference;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;
import androidx.preference.SwitchPreferenceCompat;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        setTheme(R.style.AppThemeSettings);
//        ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.setDisplayHomeAsUpEnabled(true);
//        }



        BottomNavigationView bottomNav = findViewById(R.id.bottomNavigationView);
        bottomNav.setOnNavigationItemSelectedListener(navListner);
        bottomNav.setSelectedItemId(R.id.miSettings);

    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
            DropDownPreference dropDownPreference = findPreference("alarmDropDown");
            dropDownPreference.setSummaryProvider(ListPreference.SimpleSummaryProvider.getInstance());



            Log.d("message1", "dit werkt");


        }


    }

//    public boolean onPreferenceStartFragment(PreferenceFragmentCompat caller, Preference pref) {
//        // Instantiate the new Fragment
//        final Bundle args = pref.getExtras();
//        final Fragment fragment = getSupportFragmentManager().getFragmentFactory().instantiate(
//                getClassLoader(),
//                pref.getFragment());
//        fragment.setArguments(args);
//        fragment.setTargetFragment(caller, 0);
//        // Replace the existing Fragment with the new Fragment
//        getSupportFragmentManager().beginTransaction()
//                .replace(R.id.settings, fragment)
//                .addToBackStack(null)
//                .commit();
//        return true;
//    }




    private BottomNavigationView.OnNavigationItemSelectedListener navListner =
            new com.google.android.material.bottomnavigation.BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;

                    switch(item.getItemId()){
                        case R.id.miHome:
                            Intent myIntent = new Intent(SettingsActivity.this, MainActivity.class);
                            SettingsActivity.this.startActivity(myIntent);
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                            break;

                        case R.id.miTimeline:
                            Intent timelineIntent = new Intent(SettingsActivity.this, TimelineActivity.class);
                            SettingsActivity.this.startActivity(timelineIntent);
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

                            break;

                        case R.id.miSettings:
                            break;
                    }
                    return true;
                }
            };
}