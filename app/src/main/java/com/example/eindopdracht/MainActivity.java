package com.example.eindopdracht;

import android.app.AlarmManager;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;

import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;

import java.text.DecimalFormat;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;


public class MainActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener {
    private TextView mTextView;
    private TextView smallTextView;

    NfcAdapter mAdapter;
    PendingIntent mPendingIntent;
    BottomNavigationView bottomNavigationView;

    boolean internetStatus = true;

    boolean alarmIsSet = false;
    boolean alarmIsGoingOff;
    GlobalClass globalClass;
    String android_id;
    TimePicker picker;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        android_id = Settings.Secure.getString(MainActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);
        globalClass = (GlobalClass) getApplicationContext();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("alarmRing"));
        mTextView = findViewById(R.id.textView);
        smallTextView = findViewById(R.id.smallTextView);

//        Button buttonTimePicker = findViewById(R.id.button_timepicker);

        mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timePicker = new TimePickerFragment();
                timePicker.show(getSupportFragmentManager(), "time picker");
            }
        });

        // find button for cancel alarm and call function to cancel the alarm
        Button buttonCancelAlarm = findViewById(R.id.button_cancel);
        buttonCancelAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelAlarm();
            }
        });

        // find button for stop alarm and call function to stop the alarm
        Button buttonStopAlarm = findViewById(R.id.button_stop);
        buttonStopAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopAlarm();
            }
        });

        if(getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG) != null)
        {
            onNewIntent(getIntent());
        }

        FloatingActionButton refreshButton = findViewById(R.id.button_refresh);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeNewSchedule();
            }
        });

        mAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mAdapter == null) {
            //nfc not support your device.
            return;
        }

//        openAndroidPermissionsMenu();

        //makes new intent when tag has been read
        mPendingIntent = PendingIntent.getActivity(
                this,
                0,
                new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);


        BottomNavigationView bottomNav = findViewById(R.id.bottomNavigationView);
        bottomNav.setOnNavigationItemSelectedListener(navListner);

        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        boolean firstStart = prefs.getBoolean("firstStart", true);

        //this is only happens when starting the app for the first time
//        if (firstStart) {
            makeNewUser();
//        }
    }


    private void openAndroidPermissionsMenu() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
        intent.setData(Uri.parse("package:" + this.getPackageName()));
        startActivity(intent);
    }

    //switch case for Navigation Fragment Screens
    private BottomNavigationView.OnNavigationItemSelectedListener navListner =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch(item.getItemId()){
                        case R.id.miHome:
                            break;

                        case R.id.miTimeline:
                            Intent timelineIntent = new Intent(MainActivity.this, TimelineActivity.class);
                            MainActivity.this.startActivity(timelineIntent);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            break;

                        case R.id.miSettings:
                            Intent settingIntent = new Intent(MainActivity.this, SettingsActivity.class);
                            MainActivity.this.startActivity(settingIntent);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                            break;
                    }
                    return true;
                }
            };


    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, 0);
        updateTimeText(c);
        startAlarm(c);
    }

    private void makeNewUser(){
        JSONObject jsonParam = new JSONObject();
        try {
            jsonParam.put("android_id", android_id);
            Log.d("gefaald", String.valueOf(jsonParam));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, "http://161.35.83.70/api/user/add", jsonParam, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                Log.d("gelukt", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("gefaald2", String.valueOf(error));
            }
        });
        Log.d("gefaald ", String.valueOf(jsonRequest));
        VolleySingelton.getInstance(this).addToRequestQueue(jsonRequest);
        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("firstStart", false);
        editor.apply();
    }

    private void makeNewSchedule(){
        JSONObject jsonParam = new JSONObject();
        try {
            jsonParam.put("android_id", android_id);
            jsonParam.put("hours_slept", globalClass.getTimeSlept());
//            jsonParam.put("sleep_time", "2020-08-22 22:22:22");

            Log.d("gefaald", String.valueOf(jsonParam));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, "http://161.35.83.70/api/schedule/create", jsonParam, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                internetStatus = true;
                buttonRefresh(false);

                Log.d("internettgelukt", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                internetStatus = false;
                buttonRefresh(true);

                Log.d("internettgefaald", String.valueOf(error));
            }
        });
        Log.d("gefaald ", String.valueOf(jsonRequest));
        VolleySingelton.getInstance(this).addToRequestQueue(jsonRequest);
    }

    private void updateTimeText(Calendar c) {
        String timeText = "Alarm set for: ";
        timeText += DateFormat.getTimeInstance(DateFormat.SHORT).format(c.getTime());
        globalClass.setTime(timeText);
        mTextView.setText(timeText);

    }

    private void updateTimeText() {
        mTextView.setText(globalClass.getTime());
    }


    private void startAlarm(Calendar c) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlertReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);
        //commment this out for testing purposes
        if (c.before(Calendar.getInstance())) {
            c.add(Calendar.DATE, 1);
        }

        //calculate the sleep time
        Calendar c2 = Calendar.getInstance();
        double seconds = (c.getTimeInMillis() - c2.getTimeInMillis()) / 1000;
        double hours = seconds / 3600;
        String hoursString = String.valueOf(hours);
        hoursString = hoursString.substring(0,4);

        Log.d("hours", String.valueOf(hoursString));
        float timeSlept = Float.parseFloat(hoursString);

        Log.d("hours", String.valueOf(timeSlept));

        int duration = Toast.LENGTH_SHORT;
        Toast.makeText(this, "this alarm will go off " + hoursString.substring(0,2) +" hours from now", duration).show();

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);


        globalClass.setAlarmIsSet(true);
        Log.d("something", String.valueOf(timeSlept));
        globalClass.setTimeSlept(timeSlept);

        alarmIsSet = globalClass.getAlarmIsSet();
        cancelButtonLayout(alarmIsSet);
        makeNewSchedule();
    }




    private void cancelAlarm() {
//        startAlarm();
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlertReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);
        alarmManager.cancel(pendingIntent);
        mTextView.setText("Alarm canceled");
        cancelButtonLayout(false);
        globalClass.setAlarmIsSet(false);
        globalClass.setTime("Alarm canceled");
    }


//    ignore this function made for testing purposes
    private void startAlarm(){
//        Intent i = new Intent(this, RingtonePlayingService.class);
//        this.startService(i);
        NotificationHelper notificationHelper = new NotificationHelper(this);
        //notificationHelper.stopSound();
        notificationHelper.playSound();
    }

    private void stopAlarm(){
        Log.d("bericht_stop", "alarm moet stoppen");

//        Intent i = new Intent(this, RingtonePlayingService.class);
//        stopService(i);
        NotificationHelper notificationHelper = new NotificationHelper(this);
        notificationHelper.stopSound();

        globalClass.setAlarmGoingOff(false);
        stopButtonLayout(false);
        mTextView.setText("No Alarm set");
        smallTextView.setText("");
    }

    // Our handler for received Intents. This will be called whenever an Intent
// with an action named "custom-event-name" is broadcasted.
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
//            boolean test = intent.getBooleanExtra("alarmWentOff", true);
            globalClass.setAlarmGoingOff(intent.getBooleanExtra("alarmWentOff", true));
            alarmIsGoingOff = globalClass.getAlarmGoingOff();
            Log.d("alarmIsGoingOff main", String.valueOf(alarmIsGoingOff));

            mTextView.setText("Alarm is going off");
            smallTextView.setText("Tap NFC TAG to turn off the alarm");

            stopButtonLayout(true);
            cancelButtonLayout(false);
            globalClass.setAlarmIsSet(false);
        }
    };

    @Override
    public void onResume(){
        super.onResume();
        //commented out so can be tested on emulator
        mAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);

        Log.d("internetstatus", String.valueOf(internetStatus));

        updateTimeText();
        Log.d("alarmIsGoingOff onresum", String.valueOf(globalClass.getAlarmGoingOff()));

        alarmIsGoingOff = globalClass.getAlarmGoingOff();
        if(alarmIsGoingOff){
            stopButtonLayout(true);
            cancelButtonLayout(false);
            globalClass.setAlarmIsSet(false);
        }


        alarmIsSet = globalClass.getAlarmIsSet();;
        if (alarmIsSet){
            cancelButtonLayout(true);
        }

        if(!internetStatus){
            buttonRefresh(true);
        }

    }


    @Override
    public void onPause() {
        super.onPause();
        if (mAdapter != null) {
            mAdapter.disableForegroundDispatch(this);
        }
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getTagInfo(intent);
        stopAlarm();
        Log.d("bericht", "Its reading an NFC tag");
    }

// function made in case we want to read text from the NFC tag
    private void getTagInfo(Intent intent) {
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
    }

    //this function is made so the button disappears at the right moment
    private void cancelButtonLayout(Boolean state){
        Button buttonCancelAlarm = findViewById(R.id.button_cancel);
        if(state){
            buttonCancelAlarm.setVisibility(View.VISIBLE);
        }else{
            buttonCancelAlarm.setVisibility(View.GONE);
        }
    }

    private void buttonRefresh(Boolean state){
        FloatingActionButton buttonRefresh = findViewById(R.id.button_refresh);
        TextView textRefresh = findViewById(R.id.refresh_text);

        if(state){
            buttonRefresh.setVisibility(View.VISIBLE);
            textRefresh.setVisibility(View.VISIBLE);

        }else{
            buttonRefresh.setVisibility(View.GONE);
            textRefresh.setVisibility(View.GONE);

        }
    }

    private void stopButtonLayout(Boolean state){
        Button buttonCancelAlarm = findViewById(R.id.button_stop);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean snoozeBoolean = sharedPreferences.getBoolean("snooze", false);
        if(snoozeBoolean){
            if(state){
                buttonCancelAlarm.setVisibility(View.VISIBLE);
            }else{
                buttonCancelAlarm.setVisibility(View.GONE);
            }
        }

    }

}
