package com.example.eindopdracht;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DateTimeDao {


    @Query("SELECT * FROM dateTime")
    List<DateTime> getAll();

    @Query("SELECT * FROM dateTime ORDER BY id DESC LIMIT 7")
    List<DateTime> getSome();

    @Query("SELECT * FROM dateTime LATEST")
    List<DateTime> getLatest();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void InsertDateTime(DateTime dateTime);

    @Delete
    void delete(DateTime dateTime);

    @Query("DELETE FROM datetime")
    public void nukeTable();

}
