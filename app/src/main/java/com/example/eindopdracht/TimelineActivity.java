package com.example.eindopdracht;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class TimelineActivity extends AppCompatActivity {

    GlobalClass globalClass;
    LineChart mpLineChart;
    ArrayList<Entry> entries;
    ArrayList<String> labels;
    LineDataSet set;
    LineData data;
    Context context = this;
    String baseUrl = "161.35.83.70";



    private String android_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timeline_activity);

        mpLineChart = (LineChart) findViewById(R.id.line_chart);
        mpLineChart.setNoDataText("Please use the alarm once or connect to the internet");
        entries = new ArrayList<Entry>();

        labels = new ArrayList<>();

        android_id = Settings.Secure.getString(TimelineActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);

        final DateTime[] dateTimes = new DateTime[6];




        BottomNavigationView bottomNav = findViewById(R.id.bottomNavigationView);
        bottomNav.setOnNavigationItemSelectedListener(navListner);
        bottomNav.setSelectedItemId(R.id.miTimeline);
//        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
//        dataSets.add(lineDataSet1);
//
//        LineData data = new LineData(dataSets);
//        mpLineChart.setData(data);
//        mpLineChart.invalidate();


        RequestQueue queue = VolleySingelton.getInstance(this.getApplicationContext()).getRequestQueue();
        final JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(Request.Method.GET, "http://"+baseUrl+"/api/"+android_id+"/show", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                final AppDatabase db = AppDatabase.getInstance(getApplicationContext());

                ArrayList<Entry> entries = new ArrayList<>();
                for(int i = 0; i < response.length(); i++){

                    try {
                        // Fetch From Api
                        JSONObject jsonObject = response.getJSONObject(i);


                        String id = jsonObject.getString("id");
                        String hours_slept = jsonObject.getString("hours_slept");
                        String wake_up_time = jsonObject.getString("sleep_time");
                        String created_at = jsonObject.getString("created_at");
                        Log.d("created_at", created_at.substring(5, 10));
                        created_at = created_at.substring(0, 10);

                        DateTime villager = new DateTime(hours_slept, created_at, id);

                        new Thread(new InsertDateTimeTask(db, villager)).start();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                databaseEntries();

                Log.d("gelukt", response.toString());


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("failll", String.valueOf(error));

                Toast.makeText(context, "Geen connectie met de database", Toast.LENGTH_SHORT).show();

                databaseEntries();
            }
        }


        );
//        queue.add(jsonObjectRequest);

        VolleySingelton.getInstance(this).addToRequestQueue(jsonObjectRequest);







//        db.DateTimeDao().InsertDateTime(villager);


//        new Thread(new InsertDateTimeTask(db, villager)).start();



//        Log.d("gelukt1", String.valueOf(value.get(1).getCreated_at()));
//
//        db.DateTimeDao().nukeTable();


//        new Thread(new GetDateTimeTask(db)).start();
//        db.test();

//        new Thread(new GetDateTimeTask(db)).start();



    }

    private void databaseEntries(){
        final AppDatabase db = AppDatabase.getInstance(getApplicationContext());
        GetDateTimeTask getDateTimeTask = new GetDateTimeTask(db);
        Thread thread = new Thread(getDateTimeTask);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<DateTime> value = getDateTimeTask.test();
        createGraph(value);
//        Log.d("gelukt1", String.valueOf(value.get(1).getCreated_at()));
    }

    private void createGraph(List<DateTime> dateTime){
        Collections.reverse(dateTime);
        for (int i = 0; i < dateTime.size(); i++) {
            Log.d("geluktGraph", String.valueOf(i));
            Log.d("geluktGraph", String.valueOf(dateTime.get(i).getCreated_at()));


            //Set Entries in Chart

            entries.add(new Entry(i,Float.parseFloat(dateTime.get(i).getHours_slept())));
            mpLineChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(labels));
            mpLineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

            //Styling Chart
            XAxis xAxis = mpLineChart.getXAxis();
            xAxis.setLabelRotationAngle(80);
            mpLineChart.getXAxis().setTextColor(Color.WHITE);
            mpLineChart.getLegend().setTextColor(Color.WHITE);

            YAxis yAxisRight = mpLineChart.getAxisRight();
            yAxisRight.setEnabled(false);
            mpLineChart.getAxisLeft().setTextColor(Color.WHITE);
            mpLineChart.getLegend().setEnabled(false);
            mpLineChart.getDescription().setEnabled(false);






            mpLineChart.getXAxis().setDrawGridLines(false);

            //Set Entries In Data set And Plot Chart
            labels.add(dateTime.get(i).getCreated_at());
            set = new LineDataSet(entries, "Hours Slept");
            data = new LineData(set);


            Drawable drawable = ContextCompat.getDrawable(context,R.drawable.gradiant);
            set.setFillDrawable(drawable);
            set.setDrawFilled(true);
            set.setCircleRadius(4);
            set.setDrawCircles(true);
            set.setValueTextSize(10);
            set.setLineWidth(3);
            set.setValueTextColor(Color.WHITE);
            set.setDrawValues(false);
            set.setCircleHoleColor(Color.WHITE);
            set.setColor(Color.rgb(3,218,198));


            mpLineChart.setData(data);
            mpLineChart.notifyDataSetChanged();

            mpLineChart.invalidate();
        }
    }

    //switch case for Navigation Fragment Screens
    private BottomNavigationView.OnNavigationItemSelectedListener navListner =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;

                    switch(item.getItemId()){
                        case R.id.miHome:
                            Intent homeIntent = new Intent(TimelineActivity.this, MainActivity.class);
                            TimelineActivity.this.startActivity(homeIntent);
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                            break;

                        case R.id.miTimeline:
                            break;
                        case R.id.miSettings:
                            Intent settingsIntent = new Intent(TimelineActivity.this, SettingsActivity.class);
                            TimelineActivity.this.startActivity(settingsIntent);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            break;
                    }
                    return true;
                }
            };
//    private ArrayList<Entry> dataValues1(){
//        ArrayList<Entry> dataVals =  new ArrayList<Entry>();
//        dataVals.add(new Entry(0,20));
//        dataVals.add(new Entry(1,25));
//        dataVals.add(new Entry(2,30));
//        dataVals.add(new Entry(3,40));
//        return dataVals;
//    }
}