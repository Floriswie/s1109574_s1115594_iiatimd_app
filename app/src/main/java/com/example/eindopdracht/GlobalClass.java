package com.example.eindopdracht;

import android.app.Application;

public class GlobalClass extends Application {
    private String time = "No alarm set";
    private Boolean alarmGoingOff = false;
    private Boolean alarmIsSet = false;
    private float timeSlept;

    public float getTimeSlept() {
        return timeSlept;
    }

    public void setTimeSlept(float timeSlept) {
        this.timeSlept = timeSlept;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Boolean getAlarmGoingOff() {
        return alarmGoingOff;
    }

    public void setAlarmGoingOff(Boolean alarmGoingOff) {
        this.alarmGoingOff = alarmGoingOff;
    }

    public Boolean getAlarmIsSet() {
        return alarmIsSet;
    }

    public void setAlarmIsSet(Boolean alarmIsSet) {
        this.alarmIsSet = alarmIsSet;
    }

}
