package com.example.eindopdracht;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class RingtonePlayingService extends Service  {

    static Ringtone r;

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        String alarmString = intent.getStringExtra("Dropdown");
        Log.d("Dropdown", alarmString);;

        if(alarmString.equals("Alarm")){
            //playing sound alarm
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            r = RingtoneManager.getRingtone(getBaseContext(), notification);
            //activating alarm sound
            r.play();
        }else if (alarmString.equals("Ringtone")) {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            r = RingtoneManager.getRingtone(getBaseContext(), notification);
            r.play();
        }
//        }else if (alarmString.equals("Custom")){
//
//            Uri notification = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE+"://"+ getPackageName() +"/raw/quest.mp3");
//            Log.d("dit is de route", String.valueOf(notification));
//
//            RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(), RingtoneManager.TYPE_RINGTONE, notification);
//            r = RingtoneManager.getRingtone(getBaseContext(), notification);
//            r.play();
//        }


        return START_NOT_STICKY;
    }
    @Override
    public void onDestroy()
    {
        r.stop();
    }
}
