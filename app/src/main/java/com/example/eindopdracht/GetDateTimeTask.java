package com.example.eindopdracht;

import android.util.Log;

import java.util.List;

public class GetDateTimeTask implements Runnable {
    AppDatabase db;

    private volatile List<DateTime> getHours_slept;

    public GetDateTimeTask(AppDatabase db){
        this.db = db;
    }

    @Override
    public void run(){
        getHours_slept = db.DateTimeDao().getSome();
        Log.d("getVillagerTask", String.valueOf(getHours_slept));
    }

    public List<DateTime> test (){
        return getHours_slept;
    }

}

